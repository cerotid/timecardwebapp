﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeCardWeb.EF;
using TimeCardWeb.Models;

namespace TimeCardWeb.Controllers
{
    public class TimesheetController : Controller
    {
        // GET: Timesheet
        public ActionResult Index(int id)
        {
            var data = new EmployeeTimeSheetViewModel();
            
            var db = new TimeCardDbEntities();
            var emp = db.Employees.Where(x => x.EmployeeId == id).FirstOrDefault();

            data.EmployeeId = emp.EmployeeId;
            data.EmployeeName = $"{emp.FirstName} {emp.LastName}";


            var workShifs = db.WorkShifts.Where(x => x.EmployeeId == id);
            if (workShifs != null && workShifs.Count() > 0)
            {
                foreach (var item in workShifs)
                {
                    //map to viewModel 
                    var shift = new WorkShiftViewModel();
                    shift.EmployeeId = item.EmployeeId;
                    shift.ClockOutDateTime = item.ClockOutDateTime;
                    shift.ClockInDateTime = item.ClockInDateTime;
                    shift.TimeInHours = item.ClockOutDateTime > item.ClockInDateTime
                            ? item.ClockOutDateTime.Subtract(item.ClockInDateTime).TotalHours : 0;
                    shift.WorkShiftId = item.WorkShiftId;
                    data.WorkShift.Add(shift);
                }                
            }
            return View(data);
        }

        public ActionResult ClockIn(int id)
        {
            var workShiftEntity = new WorkShift();
            workShiftEntity.EmployeeId = id;
            workShiftEntity.ClockInDateTime = DateTime.Now;
            workShiftEntity.ClockOutDateTime = workShiftEntity.ClockInDateTime;

            var db = new TimeCardDbEntities();
            db.WorkShifts.Add(workShiftEntity);
            db.SaveChanges();

            return RedirectToAction("Index", new { id = id });
        }

        public ActionResult ClockOut(int id)
        {
            var db = new TimeCardDbEntities();
            var entity = db.WorkShifts.Where(x => x.WorkShiftId == id).FirstOrDefault();
            entity.ClockOutDateTime = DateTime.Now;
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index", new { id = entity.EmployeeId });
        }

        // GET: Timesheet/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Timesheet/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Timesheet/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Timesheet/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Timesheet/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Timesheet/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Timesheet/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
