﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeCardWeb.EF;
using TimeCardWeb.Models;

namespace TimeCardWeb.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult Index()
        {
            var db = new TimeCardDbEntities();
            var employee = db.Employees;
            var data = new List<EmployeeViewModel>();
            foreach (var item in employee)
            {
                var employeeVM = new EmployeeViewModel();
                employeeVM.EmployeeId = item.EmployeeId;
                employeeVM.FirstName = item.FirstName;
                employeeVM.LastName = item.LastName;
                employeeVM.Street = item.Street;
                employeeVM.City = item.City;
                employeeVM.StateCode = item.StateCode;

                data.Add(employeeVM);
            }

            return View(data);
        }

        // GET: Employee/Details/5
        public ActionResult Details(int id)
        {
            var db = new TimeCardDbEntities();
            var employee = db.Employees.Where(x => x.EmployeeId == id).FirstOrDefault();

            var employeeVM = new EmployeeViewModel();
            employeeVM.EmployeeId = employee.EmployeeId;
            employeeVM.FirstName = employee.FirstName;
            employeeVM.LastName = employee.LastName;
            employeeVM.Street = employee.Street;
            employeeVM.City = employee.City;
            employeeVM.StateCode = employee.StateCode;

            return View(employeeVM);
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(EmployeeViewModel employeeViewModel)
        {
            try
            {
                // TODO: Add insert logic here
                if (employeeViewModel != null)
                {
                    var employeeEntity = new Employee();
                    employeeEntity.FirstName = employeeViewModel.FirstName;
                    employeeEntity.LastName = employeeViewModel.LastName;
                    employeeEntity.Street = employeeViewModel.Street;
                    employeeEntity.City = employeeViewModel.City;
                    employeeEntity.StateCode = employeeViewModel.StateCode;
                    employeeEntity.ZipCode = employeeViewModel.ZipCode;

                    var db = new TimeCardDbEntities();
                    db.Employees.Add(employeeEntity);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(EmployeeViewModel employeeViewModel)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Employee/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
