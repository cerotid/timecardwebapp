﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TimeCardWeb.Models
{
    public class WorkShiftViewModel
    {
        public int WorkShiftId { get; set; }
        public System.DateTime ClockInDateTime { get; set; }
        public System.DateTime ClockOutDateTime { get; set; }
        [DisplayFormat(DataFormatString ="{0:0.00}")]
        public double TimeInHours { get; set; }
        public Nullable<int> EmployeeId { get; set; }
    }
}