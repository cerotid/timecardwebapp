﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeCardWeb.Models
{
    public class EmployeeTimeSheetViewModel
    {
        public EmployeeTimeSheetViewModel()
        {
            this.WorkShift = new List<WorkShiftViewModel>();
        }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public List<WorkShiftViewModel> WorkShift {get; set;}
    }
}