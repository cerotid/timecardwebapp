﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeCardWeb.Models
{    
    public class EmployeeViewModel
    {
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public Nullable<int> ZipCode { get; set; }
    }
}